//
//  IDAppDelegate.h
//  SpaceShooter
//
//  Created by Ivan Fabijanovic on 01/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
